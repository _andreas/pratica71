
import static java.lang.String.format;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.binarySearch;
import static java.util.Collections.frequency;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * IF62C Fundamentos de Programação 2 Exercício de programação em Java.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica71 {

    public static void main(String[] args) {

        ArrayList<Jogador> time = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        int numeroIni = 0;

        boolean laco = true;
        System.out.println("Digite o número de jogadores: ");
        while (laco) {
            if (input.hasNextInt() && (numeroIni = input.nextInt()) >= 0) {
                System.out.println();
                laco = false;
            } else {
                System.out.println("Inválido. Digite novamente o número de jogadores:");
                input.nextLine();
            }
        }

        ArrayList<Integer> numeros = new ArrayList<>();
        int numJogador = 0;
        String nome = null;
        for (int i = 1; i <= numeroIni; i++) {
            laco = true;
            while (laco) {
                System.out.println("Número do " + i + "º jogador: ");
                if (input.hasNextInt()) {
                    numJogador = input.nextInt();
                    System.out.println();

                    if (frequency(numeros, numJogador) == 0) {
                        numeros.add(numJogador);
                        System.out.println("Nome do " + i + "º jogador: ");
                        nome = input.next();
                        time.add(new Jogador(numJogador, nome));
                        laco = false;
                    } else {
                        System.out.println("Numero já utilizado.");
                    }
                } else {
                    out.println("%n?!");
                    input.nextLine();
                }

            }
        }

        JogadorComparator comparador = new JogadorComparator();
        time.sort(comparador);

        int posicao;

        String format = "%3d %-20s";
        System.out.println("Num Nome");
        time.sort(comparador);
        for (Jogador j : time) {
            System.out.println(String.format(format, j.getNumero(), j.getNome()));
        }

        while (numJogador != 0) {
            System.out.println("Número do jogador extra: ");
            if (input.hasNextInt()) {
                numJogador = input.nextInt();
                if (numJogador == 0) {
                    break;
                }
                if ((numeros.contains(numJogador))) {
                    System.out.println("Novo nome do jogador " + numJogador + ":");
                    nome = input.next();
                    time.get(binarySearch(numeros, numJogador)).setNome(nome);
                } else {
                    numeros.add(numJogador);
                    System.out.println("Nome do jogador extra:");
                    nome = input.next();
                    time.add(new Jogador(numJogador, nome));
                    time.sort(comparador);
                }
            } else {
                out.println("!?");
                input.nextLine();
            }
        }
        System.out.println("Lista completa:");
        System.out.println("Num Nome");
        for (Jogador k : time) {
            System.out.println(String.format(format, k.getNumero(), k.getNome()));

            input.close();

        }
    }
}
